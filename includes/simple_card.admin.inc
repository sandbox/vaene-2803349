<?php
/**
 * @file
 * simple_card.admin.inc
 */

function simple_card_admin($form, &$form_state) {

  $form['simple_card_ct'] = array(
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#default_value' => drupal_map_assoc(variable_get('simple_card
    _ct', node_type_get_names())),
    '#title' => t('Content types to map?'),
  );

  return system_settings_form($form);
}