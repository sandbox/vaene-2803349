<?php
/**
 * @file
 * simple_card.menu.inc
 */


function gr8ist_menu() {
  return array(
    $items['admin/config/content/simple_card'] = array(
      'title' => 'Simple Card Config',
      'description' => 'Configuration for Simple Card',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('simple_card_admin'),
      'access arguments' => array('access administration pages'),
      'type' => MENU_NORMAL_ITEM,
      'file' => 'includes/simple_card.admin.inc',
    )
  );
}
