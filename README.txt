
Simple Card README

CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration
  * Usage
  * Node export features tips


INTRODUCTION
------------
This module creates a simple content type to act as a container for other content types. Why? The values of a card,
Title Blurb, and Image are based on the referenced node, and you can either keep the default values or override them
with custom values without affecting the referenced node. This is useful for things like heds and decks and newsletters,
where a summary of the underlying node may need to be different than the nodes summary itself. It keeps the parent node
from needing to be cluttered with specific field to cover different summaries and furthers the idea of
decoupling content from presentation. A simple Ajax implementation automatically fills in the title, blurb
and image fields which can then be overridden.

Maintainer: Randy Howk (http://drupal.org/user/134005)
Project page: http://drupal.org/project/simple_card.

Note: this module was originally built upon code from the gr8ist_card module
maintained by Randy Howk (https://www.drupal.org/user/346135) at
http://drupal.org/project/simple_card



INSTALLATION
------------
1. Copy simple_card folder to modules directory (usually sites/all/modules/custom/).
2. At admin/build/modules enable the Simple Card module in the Simple Card
   package.


For detailed instructions on installing contributed modules see:
http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
1. Enable permissions at admin/user/permissions.

2. At admin/config/content/simple_card, configure which content types you want the card to wrap,
   and then complete the mappings for each content type you choose.


USAGE
-----
1.

SIMPLE CARD FEATURES TIPS
-------------------------
